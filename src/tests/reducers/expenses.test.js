import expensesReducer from "../../reducers/expenses";
import expenses from "../fixtures/expenses";
import uuid from "uuid";

test('should set default state', () => {
    const state = expensesReducer(undefined, {type: '@@INIT'});
    expect(state).toEqual([]);
});

test('should remove expense by id', () => {
    const action = {
        type: 'REMOVE_EXPENSE',
        id: expenses[1].id
    };
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([expenses[0], expenses[2]]);
});

test('should not remove expense if id not found', () => {
    const action = {
        type: 'REMOVE_EXPENSE',
        id: -1
    };
    const state = expensesReducer(expenses, action);
    expect(state).toEqual(expenses);
});

test('should add expense', () => {
    const id = uuid();
    const expense = {
        id,
        description: 'new expense',
        note: '',
        amount: 234,
        createdAt: 0
    };
    const action = {
        type: 'ADD_EXPENSE',
        expense
    };
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([...expenses, expense]);
});

test('should edit an expense', () => {
    const id = expenses[1].id;
    const updates = {
        description: 'water bill',
        amount: 700
    };
    const action = {
        type: 'EDIT_EXPENSE',
        id,
        updates
    }
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([expenses[0], {...expenses[1], ...updates}, expenses[2]]);
});

test('should not edit an expense if id not found', () => {
    const id = -1;
    const updates = {
        description: 'water bill',
        amount: 700
    };
    const action = {
        type: 'EDIT_EXPENSE',
        id,
        updates
    }
    const state = expensesReducer(expenses, action);
    expect(state).toEqual(expenses);
});