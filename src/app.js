import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import AppRouter from "./routers/AppRouter";
import configureStore from "./store/configureStore";
import 'normalize.css/normalize.css';
import './styles/styles.scss';
import {addExpense} from "./actions/expenses";
import 'react-dates/lib/css/_datepicker.css';

const store = configureStore();

store.subscribe(() => {
    const state = store.getState();
});
store.dispatch(addExpense({description: "Water Bill", amount: 120}));
store.dispatch(addExpense({description: "Gas Bill", createdAt: 100}));
store.dispatch(addExpense({description: "Rent", amount: 109500}));

const jsx = (
    <Provider store={store}>
        <AppRouter/>
    </Provider>
);
ReactDOM.render(jsx, document.getElementById('app'));