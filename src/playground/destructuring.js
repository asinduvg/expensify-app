// const person = {
//     name: 'Asindu',
//     age: 23,
//     location: {
//         city: 'Matara',
//         temp: 92
//     }
// }
//
// const {age, name} = person
//
// console.log(`${name} is ${age}`)

// object destructring

// const book = {
//     title: 'Ego is the enemy',
//     author: 'Ryan Holiday',
//     publisher: {
//         // name: 'Penguin'
//     }
// }
//
// const {name: publisherName = 'Self Published'} = book.publisher
//
// console.log(publisherName)

// array destructuring

// const address = ['1299 S Juniper Street', 'Philadelphia', 'Pennsylvania', '19147'];
//
// const [street, city, state, zip] = address;
//
// console.log(`You are in street ${street} and city ${city}`);

const item = ['Coffee (hot)', '$2.00', '$2.50', '$2.75'];

const [drink, , price2] = item;

console.log(`A medium ${drink} costs ${price2}`);