// Higher Order Components (HOC) - A Component that renders another component

/* advantages of higher order pattern
    - reuse code
    - render hijacking
    - prop manipulation
    - abstract state
 */


import React from 'react';
import ReactDom from 'react-dom';

const Info = (props) => (
    <div>
        <h1>Info</h1>
        <p>Info is: {props.info} </p>
    </div>
);

const withAdminWarning = (WrappedComponent) => {
    return (props) => (
        <div>
            {props.isAdmin && <p>This is private info. Please don't share!</p>}
            <WrappedComponent {...props}/>
        </div>
    );
};

const requireAuthentication = (WrappedComponent) => {
    return (props) => (
        <div>
            {props.isAuthenticated ? <WrappedComponent {...props}/> : <p>Please login to view the information!</p>}
        </div>
    );
}

const AdminInfo = withAdminWarning(Info);
const AuthInfo = requireAuthentication(Info);

// ReactDom.render(<AdminInfo isAdmin={true} info="These are the details"/>, document.getElementById('app'));
ReactDom.render(<AuthInfo isAuthenticated={true} info="These are the details"/>, document.getElementById('app'));